# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 13:56:40 2021

@author: Cellot Davide
"""

#Server multithreading

import sys, signal
import http.server
import socketserver

#Leggiamo il numero di porta da riga di comando
if sys.argv[1:]:
    port = int(sys.argv[1])
else:
    port = 8080
    
#Creiamo il nostro server multi thread
server = socketserver.ThreadingTCPServer( ('', port), http.server.SimpleHTTPRequestHandler )


#Impostiamo comando Ctrl+C per uscire
def signal_handler(signal, frame):
    print('Exiting http server... (Ctrl+C pressed')
    try:
        if ( server ):
            server.server_close()
    finally:
        sys.exit(0)
        
#Ci assicuriamo che l'uscita sia pulita
server.daemon_threads = True

#Impostiamo il riuso degli indirizzi
server.allow_reuse_address = True

#Interrompiamo esecuzione in caso di Ctrl+C
signal.signal(signal.SIGINT, signal_handler)


#Creiamo il loop infinito per gestire le connessioni
try:
    while True:
        server.serve_forever()
except KeyboardInterrupt:
    pass

server.server_close()