# Elaborato di Cellot Davide

Elaborato di Programmazione di reti.

## Traccia 2

Si immagini di dover realizzare un Web Server in Python per una azienda ospedaliera.

I requisiti del Web Server sono i seguenti:

* Il web server deve consentire l’accesso a più utenti in contemporanea

* La pagina iniziale deve consentire di visualizzare la lista dei servizi erogati dall’azienda ospedaliera e per ogni servizio avere un link di riferimento ad una pagina dedicata.

* L’interruzione da tastiera (o da console) dell’esecuzione del web server deve essere opportunamente gestita in modo da liberare la risorsa socket.

* Nella pagina principale dovrà anche essere presente un link per il download di un file pdf da parte del browser.

* Come requisito facoltativo si chiede di autenticare gli utenti nella fase iniziale della connessione.


## Informazioni personali

Nome - Cognome:  Davide  Cellot

e-mail:  davide.cellot@studio.unibo.it

N° Matricola:  0000880144

## Specifiche del progetto

Questo progetto ha l'obiettivo di creare un web server in Python, rendendo visibile un sito ispirato a quello della "AUSL dell'Emilia Romagna" con degli articoli tipo.

In alto sarà presente poi una barra di navigazione contenente vari pulsanti per essere reindirizzati alle altre pagine che compongono il sito in oggetto.

Infine sul fondo di ogni pagina saranno presenti tre link appositi utili allo scaricamento di tre diversi documenti inerenti.

## Moduli-Librerie utilizzati

Ho utilizzato i moduli Python:

* sys, signal

* http.server

* socketserver

Per creare il server HTTP in Python e per settare tutte le caratteristiche utili al progetto. Inoltre con i moduli sys e signal è stato possibile creare una serie di controlli sull'operato del server utili ad avere una migliore esperienza utente.

## Utilizzo

Per prima cosa è necessario avviare il server. 
È possibile farlo sia da interfaccia grafica, aprendo il programma con un software di sviluppo (esempio. Anaconda Spider) e da lì avviarlo. 
Oppure da interfaccia utente, aprendo la console spostandosi all'interno della cartella contente il file "server.py" ed eseguendo il seguente comando:

C:\...\progetto_Reti>server.py 8080

nel quale è presente il nome del file e la porta sulla quale eseguire il server, in questo caso la classica porta 8080.

Una volta avviato il web server, il main-thread viene creato e rimane in ascolto perenne per fornire ai client una nuova connessione.

Perciò, avviato il web server è possibile aprire diversi browser che fungeranno da client facendo ogni volta la richiesta di connessione al server col seguente link:

http://localhost/8080 oppure http://127.0.0.1/8080 

Viene visualizzata la pagina indez contenente le news principali, il menu di navigazione e in basso i link per il download dei file.

Sono presenti sei link di indirizzamento, compresa la home, per navigare le diverse pagine del sito. Mentre in basso sono èresenti tre link per il download utili a:

* scaricare il modulo per la vaccinazione

* scaricare la cartella clinica

* scaricare la lista dei medici di famiglia in provincia









